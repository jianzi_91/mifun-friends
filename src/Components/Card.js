import React from 'react';
import {Grid} from '@material-ui/core';

const Card = ({id, name, email}) => {

    return (
        
        <Grid container lg={3}>
            <Grid container direction="column" justify="center" alignItems="center">
                <Grid lg={12}>
                <img alt="mifunian" src={`https://robohash.org/${id}?set=set4&200x200`}/>
                </Grid>
                <Grid lg={12}>
                <h2>{name}</h2>
                </Grid>
                <Grid>
                <p>{email}</p>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Card;