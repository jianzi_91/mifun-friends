import React from 'react';

const SearchBox = ({searchfield, searchChange}) => {

    const date = new Date();

    return (
        <div>
            <input className="pa3 ma3 ba b--black bg-lightest-blue tc grow code" type="search" placeholder="Search friends" onChange={searchChange}/>
            <p>Date: {date.toLocaleTimeString()}</p>
        </div>
    );

}

export default SearchBox;