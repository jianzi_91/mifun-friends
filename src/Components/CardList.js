import React from 'react';
import Card from './Card';
import {Grid} from '@material-ui/core';

const CardList = ({robots}) => {

    const cardArray = robots.map((user,i) => {
        return (<Card 
            key={i} 
            id={robots[i].id} 
            name={robots[i].name} 
            username={robots[i].username} 
            email={robots[i].email}/>
        );
    });

    return  (
        <Grid container spacing={1}>
            {cardArray}
        </Grid>
    );

}

export default CardList;