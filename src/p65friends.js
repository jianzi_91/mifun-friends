export const friends = [
    {
        id: 1,
        name: 'Jianzi',
        username: 'ziziiszizi',
        email: 'jianzi.lu@plus65.com.sg',
    },
    {
        id: 2,
        name: 'Chee Fai',
        username: '55699',
        email: 'cheefai.leong@plus65.com.sg',
    },
    {
        id: 3,
        name: 'Hui Sun',
        username: 'dajieda',
        email: 'huisun.lim@plus65.com.sg',
    },
    {
        id: 4,
        name: 'Leann',
        username: 'xiaomeimei',
        email: 'leann.wong@plus65.com.sg',
    },
    {
        id: 5,
        name: 'Aliy',
        username: 'aliybaba',
        email: 'aliy.akhbar@plus65.com.sg',
    },
    {
        id: 6,
        name: 'Catherine Rajadi Nyo',
        username: 'cathy',
        email: 'catherine.rajadi@plus65.com.sg',
    },
    {
        id: 7,
        name: 'Manvee',
        username: 'yewmanseng',
        email: 'manvee.yew@plus65.com.sg',
    },
    {
        id: 8,
        name: 'Raj Prem',
        username: 'laji',
        email: 'raj.prem@plus65.com.sg',
    },
    {
        id: 9,
        name: 'EE leng',
        username: 'eeleng',
        email: 'eeleng.neo@plus65.com.sg',
    },
    {
        id: 10,
        name: 'Carl',
        username: 'woshicarlbushijackson',
        email: 'carl@plus65.com.sg',
    }
];