export const mifunians = [
    {
        id: 1,
        name: 'Jianzi',
        username: 'ziziiszizi',
        email: 'jianzi.lu@mifun.my',
    },
    {
        id: 2,
        name: 'Hang',
        username: 'awkinhang',
        email: 'aw.hang@mifun.my',
    },
    {
        id: 3,
        name: 'Jie Long',
        username: 'jdragon',
        email: 'ng.jielong@mifun.my',
    },
    {
        id: 4,
        name: 'Terrance',
        username: 'feizai',
        email: 'terrance.tay@mifun.my',
    },
    {
        id: 5,
        name: 'Leon',
        username: 'laima',
        email: 'leon.mah@mifun.my',
    },
    {
        id: 6,
        name: 'KC',
        username: 'chiakc',
        email: 'chia.kc@mifun.my',
    },
    {
        id: 7,
        name: 'Summer Tan',
        username: 'superrich',
        email: 'summer.tan@mifun.my',
    },
    {
        id: 8,
        name: 'Walter Leong',
        username: 'leongshui',
        email: 'walter.leong@mifun.my',
    },
    {
        id: 9,
        name: 'Trex',
        username: 'dinosaur',
        email: 'trex.choong@mifun.my',
    },
    {
        id: 10,
        name: 'Chintu',
        username: 'blackpanther',
        email: 'chintu@mifun.my',
    }
];