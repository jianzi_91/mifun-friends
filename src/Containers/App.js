import React, { Component } from 'react';
import {connect} from 'react-redux';
import Scroll from '../Components/Scroll';
import CardList from '../Components/CardList';
import SearchBox from '../Components/SearchBox';
import { Helmet } from 'react-helmet';
import { Container} from '@material-ui/core';
import { setSearchField, requestRobots } from '../actions';
// import { requestRobots } from '../reducers';
// Smart Component

const mapStateToProps = state => {
    return {
        searchField: state.searchFriends.searchField,
        robots: state.requestRobots.robots,
        isPending: state.requestRobots.isPending,
        error: state.requestRobots.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSearchChange: (event) => dispatch(setSearchField(event.target.value)),
        onRequestRobots: () => dispatch(requestRobots())
    }
}

class App extends Component {

    componentDidMount() {
        this.props.onRequestRobots();
    }

    render() {

        // const {robots, searchfield} = this.state;
        const {searchField, onSearchChange, robots, isPending, error} = this.props;
        const filteredFriends = robots.filter(mifunian => {
            return mifunian.name.toLowerCase().includes(searchField.toLowerCase());
        });

        if (isPending) {
            return (
                <Container className="application">
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>Found Friends</title>
                        <link rel="canonical" href="http://mysite.com/example" />
                    </Helmet>
                    <div className="tc bg-dark-red">
                        <h1 className="f1 ma0 pa5 code grow">P65 Friends</h1>
                        <SearchBox searchChange={onSearchChange}/>
                        <h1>Loading...</h1>
                    </div>
                </Container>
            )
        }
        else {
            return (
                <div className="application">
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>P65 Friends</title>
                        <link rel="canonical" href="http://mysite.com/example" />
                    </Helmet>
                    <Container className="tc bg-dark-red">
                        <h1 className="f1 ma0 pa5 code grow">P65 Friends</h1>
                        <SearchBox searchChange={onSearchChange}/>
                        <Scroll>
                            <CardList robots={filteredFriends}/>
                        </Scroll>
                    </Container>
                </div>
            );
        }
        
    }

} 

export default connect(mapStateToProps, mapDispatchToProps)(App);